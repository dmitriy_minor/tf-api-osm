"""
Модуль считывает конфигурацию и внутренность тестов.
После чего запускает тесты.
"""

import sys
import json

from collections import OrderedDict

from modules.core.logger import create_logger
from modules.core.test_report import TestReport
from modules.core.test_validator import Validator
from modules.core.argument_parser import argument_parser
from modules.procedure.direct_geocoding import DirectGeocoding
from modules.procedure.reverse_geocoding import ReverseGocoding

from modules.core import fs_worker as fs

__author__ = 'Минор Д.С'
__all__ = ['main']

SWITCH_PROCEDURE = {
    "DirectGeocoding": DirectGeocoding,
    "ReverseGeocoding": ReverseGocoding
}

def main():
    """
    Обрабатывает агрументы из командной строки.
    Загружает содержимое конфигурационного файла .
    Создает объект logger, для логирования утилиты.
    Загружает содержимое теста.
    Проводит валидацию теста, вызывая класс Validator.
    Вызывает тесты. После их прохождения, выводит результат прохождения тестов.
    """

    if sys.version_info < (3, 5):
        print("Error. Use python 3.5 or greater")
        sys.exit(1)

    arg_parser = argument_parser()
    namespace = arg_parser.parse_args()
    test_file = namespace.test_file.read()
    test_number = namespace.test_number
    config_file = namespace.config_file.read()
    log_mode = namespace.log_mode

    config_file_data = json.loads(config_file, object_pairs_hook=OrderedDict)
    test_report = TestReport()

    fs_work = fs.FsWorking()

    logger = create_logger(config_file_data["PathLog"], fs_work, log_mode)

    logger.info("Reading JSON script...")
    try:
        test_desc = json.loads(test_file, object_pairs_hook=OrderedDict)
    except (ValueError, KeyError, TypeError):
        logger.error("Wrong JSON format. Detail: %s", sys.exc_info()[1])
        sys.exit(1)

    validator = Validator(test_desc, config_file_data["PathJsonSchema"])
    if validator.validation_test():
        logger.info("Test successful validate")

    for num, test in enumerate(test_desc.get("TestCase")):
        if test_number != num and test_number is not None:
            continue
        test_keys_name = test.get("Name")
        logger.info(f"Start test {test_keys_name}")
        for procedures in test.get("TestProcedure"):
            for procedure_name, procedure_param in procedures.items():
                for test_param in procedure_param:
                    procedure = SWITCH_PROCEDURE[procedure_name](**test_param)
                    procedure.get_response()
                    test_report.add_test_procedure(test_keys_name, procedure.result)

    logger.info("--------Test suite result---------")
    test_report.show_in_log()

if __name__ == '__main__':
    main()