"""
Модуль отправляют запрос на API OSM c заданными в тесте значениями широты и долготы, производит поиск локации в ответе.
Ищет совпадение заданной локации с той, что нашел в ответе.
После чего принимает решение о прохождении теста. Передает результат в класс TestResult.
"""

import json
import logging
import requests

from xml.etree import ElementTree

from modules.core.test_result import TestResult

__author__ = 'Минор Д.С'
__all__ = ["ReverseGocoding"]

logger = logging.getLogger("tester")


class ReverseGocoding:
    """
    Отправляет запрос на API OSM с известной широтой и долготой.
    Парсит вывод в зависимости от заданного формата, находит в нем локацию и сравнивает ее с заданной в тесте.
    В зависимости от условий теста, предоставляет соответсвующий результат классу TestResult.
    """

    URL = "https://nominatim.openstreetmap.org/reverse?format={}&lat={}&lon={}&zoom={}&addressdetails=1"

    def __init__(self, lat: str, lon: str, location: str, wait_result: bool, zoom: int, format_ : str ='xml'):
        """
        Конструктор в котором хранятся глобальные переменные класса

        :param lat: Передаваемое значение ширины.
        :param lon: Передаваемое значение долготы.
        :param location: Ожидаемая локации.
        :param wait_result: Ожидаемое значение результата теста.
        :param zoom: Масштаб поиска локации
        :param format_: Формат запроса.
        """

        self._lat_value = lat
        self._lon_value = lon
        self._location_value = location
        self._wait_result = wait_result
        self._zoom = zoom
        self._format = format_

    def get_response(self):
        """ Отправляет запрос на API nominatim.openstreetmap.org с известными координатами."""

        response = requests.get(self.URL.format(self._format, self._lat_value, self._lon_value, self._zoom))
        print(self.URL.format(self._format, self._lat_value, self._lon_value, self._zoom))
        if response.status_code != 200:
            logger.error(f"Не верный код ответа: {response.status_code}")
            result = False
            if not self._wait_result and not result:
                self.result = TestResult(True, f"Не верный код ответа: {response.status_code}")
            else:
                self.result = TestResult(False, f"Не верный код ответа: {response.status_code}")
            return
        self.parser(response)

    def parser(self, response: requests):
        """
        На основе формата, решает какой метод с парсером вызвать.

        :param response: Ответ API OSM.
        """

        if self._format == 'xml':
            self.xml_parser(response.content)
        elif self._format == 'json':
            self.json_parser(response.content)
        elif self._format == 'jsonv2':
            self.json_parser(response.content)
        elif self._format == 'geojson':
            self.geojson_parser(response.content)
        elif self._format == 'geocodejson':
            self.geocodejson_parser(response.content)

    def xml_parser(self, body: bytes):
        """
        XML парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_str = ElementTree.ElementTree(body.decode()).getroot()
        body_xml = ElementTree.fromstring(body_str)
        found_location = body_xml[0].text
        self.result_analysis(found_location)

    def json_parser(self, body: bytes):
        """
        JSON парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_json = json.loads(body.decode())
        found_location = body_json.get("display_name")
        if found_location is None:
            found_location = body_json.get("error")
        self.result_analysis(found_location)

    def geojson_parser(self, body: bytes):
        """
        GeoJSON парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_json = json.loads(body.decode())
        try:
            found_location = body_json.get("features")[0].get("properties").get("display_name")
        except TypeError:
            found_location = body_json.get("error")
        self.result_analysis(found_location)

    def geocodejson_parser(self, body: bytes):
        """
        GeocodeJSON парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_json = json.loads(body.decode())
        try:
            found_location = body_json.get("features")[0].get("properties").get("geocoding").get("label")
        except TypeError:
            found_location = body_json.get("error")
        self.result_analysis(found_location)

    def result_analysis(self, found_location):
        """
        Сравнивает полученные значения локации с заданной в тесте.
        На основе ожидаемого результата теста, передает его в TestResult

        :param found_location: Найденная локация.
        """

        logger.debug(f"Lat and Lon: {self._lat_value}:{self._lon_value}")
        logger.debug(f"---| Expected value local '{self._location_value}', received value '{found_location}'")
        if self._location_value == str(found_location):
            logger.info("The expected value is equal to the received")
            result = True
        else:
            result = False

        if self._wait_result and result:
            self.result = TestResult(True)
        elif not self._wait_result and not result:
            self.result = TestResult(True)
        else:
            self.result = TestResult(False, f"Совпадений не найдено")