"""
Модуль отправляют запрос на API OSM c заданным в тесте значением локации, производит парсинг ответа, ищет совпадения по долготе и широте.
После чего принимает решение о прохождении теста. Передает результат в класс TestResult.
"""
import json
import logging
import requests

from xml.etree import ElementTree
from modules.core.test_result import TestResult

__author__ = 'Dmitriy Minor'
__all__ = ['DirectGeocoding']

logger = logging.getLogger("tester")


class DirectGeocoding:
    """
    Отправляет запрос на API OSM с известной локацией.
    Парсит вывод в зависимости от заданного формата, находит в нем координаты и сравнивает их с заданными в тесте.
    В зависимости от условий теста, предоставляет соответсвующий результат классу TestResult.
    """

    URL = "https://nominatim.openstreetmap.org/search{}format={}&polygon_geojson=1&addressdetails=1"

    def __init__(self, lat: int, lon: int, location: str, wait_result: bool, format_ : str ='xml'):
        """
        Конструктор в котором хранятся глобальные переменные класса

        :param lat: Ожидаемое значение ширины.
        :param lon: Ожидаемое значение долготы.
        :param location: Передаваемое значение локации.
        :param wait_result: Ожидаемое значение результата теста.
        :param format_: Формат запроса.
        """
        self._lat_value = lat
        self._lon_value = lon
        self._location = location
        self._wait_result = wait_result
        self._format = format_
        self.result = None

    def get_response(self):
        """ Отправляет запрос на API nominatim.openstreetmap.org с известной локацией"""

        response = requests.get(self.URL.format(self._location, self._format))
        if response.status_code != 200:
            logger.error(f"Не верный код ответа: {response.status_code}")
            result = False
            if not self._wait_result and not result:
                self.result = TestResult(True, f"Не верный код ответа: {response.status_code}")
            else:
                self.result = TestResult(False, f"Не верный код ответа: {response.status_code}")
            return
        self.parser(response)

    def parser(self, response: requests):
        """
        На основе формата, решает какой метод с парсером вызвать.

        :param response: Ответ API OSM.
        """

        if self._format == 'xml':
            self.xml_parser(response.content)
        elif self._format == 'json':
            self.json_parser(response.content)
        elif self._format == 'jsonv2':
            self.json_parser(response.content)

    def xml_parser(self, body: bytes):
        """
        XML парсер тела ответа API OSM. Ищет в ответе значения ширины и долготы.

        :param body: Тело ответ API OSM.
        """

        search_results = []
        body_string = ElementTree.ElementTree(body.decode()).getroot()
        body_xml = ElementTree.fromstring(body_string)
        for child in body_xml:
            search_results.append(
                {
                    "lat": child.attrib.get('lat'),
                    "lon": child.attrib.get('lon')
                }
            )
        self.result_analysis(search_results)

    def json_parser(self, body: bytes):
        """
        JSON парсер тела ответа API OSM. Ищет в ответе значения ширины и долготы.

        :param body: Тело ответ API OSM.
        """

        search_results = []
        body_json = json.loads(body.decode())
        for item in body_json:
            search_dict = {"lat": item.get("lat"), "lon": item.get("lon")}
            search_results.append(search_dict)
        self.result_analysis(search_results)

    def result_analysis(self, search_results: list):
        """
        Сравнивает полученные значения широты и долготы с заданными в тесте.
        На основе ожидаемого результата теста, передает его в TestResult

        :param search_results: Список с широтой и долготой, найденных локаций.
        """

        logger.debug(f"Location: {self._location}")
        result = None
        for num, ind in enumerate(search_results, 1):
            lat = ind.get("lat")
            lon = ind.get("lon")
            logger.debug(f'Match number {num}:')
            logger.debug(f"---| Expected value lat {self._lat_value}, received value {lat}")
            logger.debug(f"---| Expected value lon {self._lon_value}, received value {lon}")
            if str(self._lat_value) == lat and str(self._lon_value) == lon:
                logger.info("The expected value is equal to the received")
                result = True
                break
            result = False
        if self._wait_result and result:
            self.result = TestResult(True)
        elif not self._wait_result and not result:
            self.result = TestResult(True)
        else:
            self.result = TestResult(False, f"Совпадений не найдено")
