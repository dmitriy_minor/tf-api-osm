""" Модуль предназначенный для работы с файловой системой."""

import os
import sys
import logging

__author__ = 'Минор Д.С'
__all__ = ['FsWorking']

logger = logging.getLogger("tester")

class FsWorking:
    """ Используется для работы с файлами и папками логов"""

    def create_log_dir(self, log_path: str):
        """
        Парсит аргументы, переданные из командной строки.

        :param log_path: Путь до папки, в которой будут хранится логи тестов

        :return True
        """

        try:
            os.makedirs(log_path)
        except FileExistsError:
            if not self.clear_log_dir(log_path):
                return False
        except PermissionError:
            logger.error("Сan't create log folder. Detail: %s",sys.exc_info()[1])
            return False
        return True

    def clear_log_dir(self, log_path: str):
        """
        Подчистка папки с логами.

        :param log_path: Путь до папки, в которой будут хранится логи тестов
        """

        for file in os.listdir(log_path):
            try:
                file = log_path + "/" + file
                if file != "" and os.path.isfile(file):
                    os.remove(file)
            except PermissionError:
                logger.error("Сan't clear log folder. Detail: %s",sys.exc_info()[1])
                return False
        return True
