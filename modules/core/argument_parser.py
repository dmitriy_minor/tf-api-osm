"""Модуль обрабатывает аргументы, переданные из командной строки."""

import argparse

__author__ = 'Минор Д.С'
__all__ = ['argument_parser']

def argument_parser() -> argparse.ArgumentParser:
    """
    Парсит аргументы, переданные из командной строки.

    :return new_parser: Объект argparse.ArgumentParser
    """

    new_parser = argparse.ArgumentParser()
    new_parser.add_argument('--log_mode', type=str, required=False)                         #Уровень логирования
    new_parser.add_argument('-t', '--test_file', type=argparse.FileType(), required=True)   #Путь до файла с Test suite
    new_parser.add_argument('-n', '--test_number', type=int, required=False)                #Номер запускаемого Test case
    new_parser.add_argument('-c', '--config_file', type=argparse.FileType(), required=True) #Путь до конфигурационного файла
    return new_parser
