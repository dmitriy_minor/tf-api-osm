"""Модуль валидирующий тестовый файл."""

import os
import sys
import json
import logging
import jsonschema

__author__ = 'Минор Д.С'
__all__ = ['Validator']

logger = logging.getLogger("tester")

class Validator:
    """Используется для валидации файлов с тестами. Валидация производится по JSON схеме."""
    
    def __init__(self, json_file: dict, schemas_directory: str):
        """
        Хранит в себе глобальные переменные класса.

        :param json_file: Полный путь до файла с тестом
        :param schemas_directory: Полный путь до папки с JSON схемами процедур
        """

        self.schemas_directory = schemas_directory
        self.TestName = None
        self.Tests = []
        self.JsonFile = json_file

    def validate_procedure(self, procedure: str, schema_name: str):
        """
        Ищет подходящую JSON схему для заданной процедуры. После чего производит валидацию.

        :param procedure: Процедура, которую требуется валидировать по JSON схеме
        :param schema_name: Название JSON схемы, которая должна быть в папке с схемами
        """

        for schema_file in os.listdir(self.schemas_directory):
            if schema_name == schema_file:
                file_path = self.schemas_directory + schema_file
                try:
                    file = open(file_path, "r", encoding="utf-8")
                except FileNotFoundError as error:
                    logger.error(error)
                    sys.exit(1)
                try:
                    schema = json.loads(file.read())
                    jsonschema.validate(procedure, schema)
                    break
                except jsonschema.exceptions.ValidationError as error:
                    logger.error(error)
                    sys.exit(1)
                except json.decoder.JSONDecodeError:
                    file.close()
                    logger.error(f"Wrong schema format in {schema_file} file.")
                    sys.exit(1)
        else:
            logger.error(f"Not found JSON schema {schema_name}")
            sys.exit(1)

    def validation_test(self):
        """
        Метод, который валидирует файл с Test suite.
        В случае успеха вадидации глобальных секций в Test suite, вызывается метод validate_procedure для валидации отдельных процедур.
        """
        if not "TestName" in self.JsonFile:
            logger.error("Validation error in global section: TestName is a required property")
            sys.exit(1)

        self.TestName = self.JsonFile.get("TestName")

        if not "TestCase" in self.JsonFile:
            logger.error("Validation error in global section: TestCase is a required property")
            sys.exit(1)

        self.Tests = self.JsonFile.get("TestCase")
        for num, test in enumerate(self.Tests):
            if "Name" in test:
                if type(test["Name"]) != str or len(test["Name"]) == 0:
                    logger.error(f"Validation error in section Tests, test number {num}: Name must be non-zero string")
                    sys.exit(1)
            else:
                logger.error(f"Validation error in section Tests, test number {num}: Name is a required property")
                sys.exit(1)
            if "Description" in test:
                if type(test["Description"]) != str or len(test["Description"]) == 0:
                    logger.error(f"Validation error in section Tests, test number {num}: Description must be non-zero string")
                    sys.exit(1)
            else:
                logger.error(f"Validation error in section Tests, test number {num}: Description is a required property")
                sys.exit(1)
            if not "TestProcedure" in test:
                logger.error(f"Validation error in section Tests, test number {num}: TestProcedure is a required property")
                sys.exit(1)
            else:
                for procedure in test["TestProcedure"]:
                    if "DirectGeocoding" in procedure:
                        self.validate_procedure(procedure["DirectGeocoding"], "DirectGeocoding")
                    elif "ReverseGeocoding" in procedure:
                        self.validate_procedure(procedure["ReverseGeocoding"], "ReverseGeocoding")
