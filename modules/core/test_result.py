""" Результат тестов"""

import logging

__author__ = 'Минор Д.С'
__all__ = ["TestResult"]

logger = logging.getLogger("tester")


class TestResult:
    """ Хранит в себе результат тест кейсов и описание ошибки, если тест не прошел."""


    def __init__(self, status: bool, error_description: str = None):
        """
        Конструктор класса.
        :param status: Успех или не успех теста.
        :param error_description: Описание ошибки.
        """

        self.status = status
        self.error_description = error_description
