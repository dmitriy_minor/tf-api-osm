"""Модуль создающая логгер"""
import sys
import logging
import datetime

__author__ = 'Минор Д.С'
__all__ = ['create_logger']


def create_logger(log_path: str, fs_work: object, log_mode: str = "INFO"):
    """
    Создает logger. Логирование происходит как в файл, так и в консоль.

    :param fs_work: Объект класса FsWorking
    :param log_path: Путь до папки, в которой будут хранится логи тестов
    :param log_mode: Уровень логирования. По дефолту INFO.

    :return logger: Возвращает объект логгер
    """

    now = datetime.datetime.now()
    log_path = "/".join((log_path, now.strftime("%Y_%m_%d_%H_%M_%S"),))
    if not fs_work.create_log_dir(log_path):
        sys.exit(1)
    log_file = log_path + "/test.log"
    if log_mode == "DEBUG":
        logging.basicConfig(filename=log_file,
                            format=u'%(asctime)-8s %(levelname)-8s [%('u'module)s:%(lineno)d] %(message)-8s',
                            filemode='w', level=logging.DEBUG)
    else:
        logging.basicConfig(filename=log_file,
                            format=u'%(asctime)-8s %(levelname)-8s [%('u'module)s:%(lineno)d] %(message)-8s',
                            filemode='w', level=logging.INFO)
    logger = logging.getLogger("tester")

    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger
