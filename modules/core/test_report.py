""" Модуль выводящий отчет в консоль."""

import logging

__author__ = 'Минор Д.С'
__all__ = ["TestReport"]

logger = logging.getLogger("tester")


class TestReport:
    """ Собирает результаты тестов. Выводит результаты в консоль."""

    def __init__(self):
        """ Хранит в себе результаты тестов."""

        self._test_procedure = []

    def add_test_procedure(self, test_name: str, result: bool):
        """
        Добавляет результат теста в список с тестами.

        :param test_name: Имя тест кейса.
        :param result: Результат тест кейса.
        """

        self._test_procedure.append({"TestName": test_name, "Result": result})

    def show_in_log(self):
        """
        Выводит информацию о результатах тест кейсов в консоль.
        """
        for num, procedure in enumerate(self._test_procedure, 1):
            logger.info(
                f"Result Test Case: {procedure['TestName']}. "
                f"Test number {num} have result "
                f"{'SUCCESSFUL' if procedure['Result'].status else 'FAIL'}."
                f"{' Details: ' + procedure['Result'].error_description if not procedure['Result'].status else ''}"
            )
